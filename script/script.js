
//Обьект
const calc = {
   firstNumber: "",
   secondNumber: "",
   result: "",
   operand: "",
   addMemory: "",
   finish: false
}

// Отображение
function show(value, el) {
   el.value = value
}

//Берем элементы по id
const getId = id => document.getElementById(id);

window.addEventListener("DOMContentLoaded", () => {
   //Все кнопки
   const btn = document.querySelector(".keys"),
      //Экран калькулятора
      display = document.querySelector(".display > input")

   //Функция очистки экрана
   function clear() {
      calc.firstNumber = '';
      calc.secondNumber = '';
      calc.operand = '';
      display.textContent = 0;
      show(display.textContent, display);

   }
   //Запуск функции очистки экрана
   getId('buttonC').onclick = clear;

   //Нажатие по кнопкам
   btn.addEventListener("click", function (e) {
      let el = e.target;
      //Если нажата не кнопка
      if (!el.classList.contains("button")) return;
      //Если нажата C
      if (el.classList.contains("clear")) return;
      

      // Получаю знак
      if (el.value === "-" || el.value === "+" || el.value === "/" || el.value === "*" || el.value === "m+" || el.value === "m-" || el.value === "mrc") {
         calc.operand = el.value;
      }

      //Получаю  первое число
      if (calc.secondNumber === "" && calc.operand === "") {
         calc.firstNumber += el.value;

         //Отоброжаю на экран
         show(calc.firstNumber, display)
         //Получаю второе число
      } else if (calc.operand !== "") {
         calc.secondNumber += el.value;

         //Отоброжаю на экран
         show(calc.secondNumber.replace(/[^0-9, .]/g, ""), display)
      }

      //Отображаю на экране только число при нажатии оператора
      if (el.value === "-" || el.value === "+" || el.value === "/" || el.value === "*" || el.value === "m+" || el.value === "m-" || el.value === "mrc") {
         show(calc.firstNumber, display)
      }

      //Записываю число в память
      if (el.value === "m+") {
         calc.addMemory = display.value;
      } else if (el.value === "m-") { //Показываю число
         show(calc.addMemory, display)
         calc.firstNumber = calc.addMemory;
         calc.secondNumber = '';
         calc.operand = '';
         calc.result = '';
      } else if (el.value === "mrc") { //Удаляю число
         calc.addMemory = '';
      }

      //Операция
      if (el.value === "=") {
         switch (calc.operand) {
            case "+":
               calc.result = parseFloat((+calc.firstNumber)) + parseFloat((+calc.secondNumber.replace(/[^0-9, .]/g, "")));
               break;
            case "-":
               calc.result = parseFloat(calc.firstNumber) - parseFloat(calc.secondNumber.replace(/[^0-9, .]/g, ""));
               break;
            case "/":
               calc.result = parseFloat(calc.firstNumber) / parseFloat(calc.secondNumber.replace(/[^0-9, .]/g, ""));
               break;
            case "*":
               calc.result = parseFloat(calc.firstNumber) * parseFloat(calc.secondNumber.replace(/[^0-9, .]/g, ""));
               break;
         }
         show(calc.result, display);
         calc.firstNumber = calc.result;
         calc.secondNumber = '';
         calc.operand = '';

      }

   })
});






